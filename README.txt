﻿This is a very simple plugin for Confluence that adds two new remote methods 
(accessible via SOAP and XML-RPC) for administrators to force a specific user 
to be synchronised into Confluence when Confluence is configured to connect to 
a remote LDAP server using Delegated Authentication.

If you don't know what this means, then this plugin is probably not going to be 
useful for you - it fills a very specific need that really ought to just be 
part of Confluence core. :-)

The two methods you can use are:
* forceSync(String login_token, String username) - looks for the specified 
username in any remote, delegated LDAP directories and copies the user in to 
Confluence if it is found.
* fakeLogin(String login_token, String username) - triggers a fake login event 
for the specified username. You can use this method to force the creation of 
the user's 'PersonalInformationObject' in the database, which is used in places 
such as the Confluence People Directory and @mentions

The SOAP endpoint for this plugin is accessible at:
 ${CONFLUENCE_URL}/rpc/soap-axis/usersync?wsdl
The XML-RPC endpoint for this plugin is accessible at:
 ${CONFLUENCE_URL}/rpc/xml-rpc/usersync

You'll need to know how to invoke a remote SOAP or XML-RPC service from your 
language of choice in order to be able to use this plugin.

If you have any problems with it, please contact me on twitter (@jaysee00) or 
email me (jclark at atlassian dot com).
