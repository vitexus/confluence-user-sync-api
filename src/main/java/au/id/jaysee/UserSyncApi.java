package au.id.jaysee;

import com.atlassian.confluence.rpc.RemoteException;
import com.atlassian.confluence.rpc.SecureRpc;

/**
 *
 */
public interface UserSyncApi extends SecureRpc
{
    public boolean forceSync(String token, String username) throws RemoteException;

    public boolean fakeLogin(String token, String username) throws RemoteException;
}
