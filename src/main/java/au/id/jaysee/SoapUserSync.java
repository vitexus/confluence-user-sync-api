package au.id.jaysee;

import com.atlassian.confluence.event.events.security.LoginEvent;
import com.atlassian.confluence.event.events.security.LogoutEvent;
import com.atlassian.confluence.rpc.AuthenticationFailedException;
import com.atlassian.confluence.rpc.RemoteException;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.crowd.EmbeddedCrowdBootstrap;
import com.atlassian.crowd.dao.application.ApplicationDAO;
import com.atlassian.crowd.directory.DelegatedAuthenticationDirectory;
import com.atlassian.crowd.directory.RemoteDirectory;
import com.atlassian.crowd.directory.loader.DirectoryInstanceLoader;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.DirectoryType;
import com.atlassian.crowd.embedded.atlassianuser.EmbeddedCrowdUser;
import com.atlassian.crowd.exception.ApplicationNotFoundException;
import com.atlassian.crowd.exception.DirectoryInstantiationException;
import com.atlassian.crowd.exception.OperationFailedException;
import com.atlassian.crowd.exception.UserNotFoundException;
import com.atlassian.crowd.model.application.Application;
import com.atlassian.crowd.model.application.DirectoryMapping;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.sal.api.component.ComponentLocator;
import com.atlassian.sal.api.transaction.TransactionCallback;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import com.atlassian.user.User;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 */
public class SoapUserSync implements UserSyncApi
{
    private static final Logger log = LoggerFactory.getLogger(SoapUserSync.class);

    private final TransactionTemplate transactionTemplate;
    private final ApplicationDAO applicationDAO;
    private final DirectoryInstanceLoader directoryLoader;
    private final PermissionManager permissionManager;
    private final EventPublisher eventPublisher;

    public SoapUserSync(TransactionTemplate transactionTemplate, PermissionManager permissionManager, EventPublisher eventPublisher)
    {
        this.transactionTemplate = transactionTemplate;
        this.permissionManager = permissionManager;
        this.eventPublisher = eventPublisher;
        this.applicationDAO = ComponentLocator.getComponent(ApplicationDAO.class, "embeddedCrowdApplicationDao");
        this.directoryLoader = ComponentLocator.getComponent(DirectoryInstanceLoader.class, "directoryInstanceLoader");
    }

    @Override
    public boolean forceSync(String token, String username) throws RemoteException
    {
        ensureAdministrator();
        return getLdapUser(username) != null;
    }

    @Override
    public boolean fakeLogin(String token, String username) throws RemoteException
    {
        ensureAdministrator();
        eventPublisher.publish(new LoginEvent(this, username, "", "", ""));
        eventPublisher.publish(new LogoutEvent(this, username, ""));

        return true;
    }

    private void ensureAdministrator() throws RemoteException
    {
        if (!permissionManager.hasPermission(AuthenticatedUserThreadLocal.getUser(), Permission.ADMINISTER, PermissionManager.TARGET_SYSTEM))
            throw new RemoteException("You must be a Confluence Administrator to synchronise LDAP users");
    }

    // This code was copied from https://bitbucket.org/jaysee00/example-confluence-sso-authenticator
    protected User getLdapUser(final String username)
    {
        // If we do find the user in an LDAP directory, write operations on the Confluence database may be caused in order
        // to copy the new user into an internal directory (for delegated auth directories) and synchronise group memberships.
        // We need to wrap this code in a new read/write transaction in order for this to work
        return transactionTemplate.execute(new TransactionCallback<EmbeddedCrowdUser>()
        {
            @Override
            public EmbeddedCrowdUser doInTransaction()
            {
                final Application application;
                try
                {
                    application = applicationDAO.findByName(EmbeddedCrowdBootstrap.APPLICATION_NAME);
                }
                catch (ApplicationNotFoundException e)
                {
                    // Unable to load the Application singleton from Embedded Crowd; something is seriously wrong.
                    log.error(String.format("Unable to load Application singleton %s: %s", e.getMessage(), e.toString()));
                    return null;
                }

                // Iterate through the application's configured directories and search any LDAP-based directory for the desired user
                Iterable<Directory> activeDirectories = getActiveLdapDirectories(application);
                for (Directory dir : activeDirectories)
                {
                    try
                    {
                        log.debug(String.format("Enumerating directory %s (%s): %s", String.valueOf(dir.getId()), dir.getName(), dir.getDescription()));
                        log.debug(String.format("The directory is of type %s", dir.getType().toString()));

                        // Retrieve the corresponding remote directory object.
                        RemoteDirectory remoteDir;
                        remoteDir = directoryLoader.getDirectory(dir);

                        // If the directory is configured for Delegated LDAP Authentication, we may need to handle the situation
                        // where this is the first time the user has logged in and the directory is configured to "Copy User On Login".
                        // We need to tell the directory to copy the user into the internal directory.
                        com.atlassian.crowd.model.user.User crowdUser;
                        if (remoteDir instanceof DelegatedAuthenticationDirectory)
                        {
                            log.debug("Forcing optional 'Copy User on Login' and 'Group Import' processing.");
                            // This call may cause write operations on the Confluence database in order to copy the new user and
                            // sync group memberships.
                            crowdUser = ((DelegatedAuthenticationDirectory) remoteDir).addOrUpdateLdapUser(username);
                        }
                        else
                        {
                            log.debug("Locating user in directory");
                            crowdUser = remoteDir.findUserByName(username);
                        }

                        return new EmbeddedCrowdUser(crowdUser); // Wrap up the crowd user object in a principal that can be injected into the Session.
                    }
                    catch (DirectoryInstantiationException e)
                    {
                        log.error(String.format("Unable to instantiate the desired RemoteDirectory; skipping (%s: %s)", e.getMessage(), e.toString()));
                    }
                    catch (UserNotFoundException e)
                    {
                        log.debug("User not found in this directory; skipping.");
                    }
                    catch (OperationFailedException e)
                    {
                        log.error(String.format("Failed to check directory for user; skipping (%s: %s)", e.getMessage(), e.toString()));
                    }
                }

                log.info("The requested username does not appear to be a valid user in any configured LDAP directory.");
                return null;
            }
        });
    }

      /**
     * Returns all configured LDAP Directories and Delegated LDAP Auth Directories that are not currently disabled. The
     * set of directories are theoretically returned in the correct priority order.
     *
     * @param application The Embedded Crowd {@link Application} singleton.
     * @return A set of {@link Directory} objects that match the desired criteria.
     */
    private Iterable<Directory> getActiveLdapDirectories(final Application application)
    {
        return Iterables.filter(Iterables.transform(application.getDirectoryMappings(), new Function<DirectoryMapping, Directory>()
        {
            public Directory apply(final DirectoryMapping from)
            {
                return from.getDirectory();
            }
        }), new Predicate<Directory>()
        {
            public boolean apply(final Directory from)
            {
                return (from.isActive() && (from.getType().equals(DirectoryType.DELEGATING) || from.getType().equals(DirectoryType.CONNECTOR)));
            }
        });
    }

    @Override
    public String login(String username, String password) throws AuthenticationFailedException, RemoteException
    {
        // IGNORE: Handled by interceptor.
        return null;
    }

    @Override
    public boolean logout(String token) throws RemoteException
    {
        // IGNORE: Handled by interceptor.
        return true;
    }
}
